FROM node:12
ADD ./source /opt
WORKDIR /opt
RUN npm install
EXPOSE 4200
ENTRYPOINT ["/usr/local/bin/npm", "run", "start", "--", "--host", "0.0.0.0"]
